WORKSPACE := $(shell terraform workspace show)
BUCKET := $(shell cat backend.tf | grep bucket | awk '{print $$3}')
TABLE := $(shell cat backend.tf | grep dynamodb_table | awk '{print $$3}')
MAKEFILE_DIR := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

ifeq ($(WORKSPACE),default)
	REMOTE_STATE_PREFIX := "terraform.tfstate"

	REMOTE_TFVARS_PATH := "s3://$(BUCKET)/terraform.tfvars"
	LOCAL_TFVARS_PATH := "$(MAKEFILE_DIR)/tfvars/terraform.tfvars"
else
	REMOTE_STATE_PREFIX := "env:/$(WORKSPACE)/terraform.tfstate"

	REMOTE_TFVARS_PATH := "s3://$(BUCKET)/env:/$(WORKSPACE)/terraform.tfvars"
	LOCAL_TFVARS_PATH := "$(MAKEFILE_DIR)/tfvars/$(WORKSPACE)/terraform.tfvars"
endif

push_vars:
	aws s3 cp --sse="AES256" $(LOCAL_TFVARS_PATH) $(REMOTE_TFVARS_PATH)

pull_vars:
	aws s3 cp --sse="AES256" $(REMOTE_TFVARS_PATH) $(LOCAL_TFVARS_PATH)

state_versions:
	$(eval STATE_VERSIONS := '$(shell aws s3api list-object-versions --bucket $(BUCKET) --prefix $(REMOTE_STATE_PREFIX) --max-items 10 | jq -rc .Versions)')
	#echo $(STATE_VERSIONS)

state_restore_previous: state_versions
	$(eval PREVIOUS_VERSION := '$(shell echo $(STATE_VERSIONS) | jq -rc '.[1]')')
	$(eval PREVIOUS_VERSION_ID := '$(shell echo $(PREVIOUS_VERSION) | jq -rc '.VersionId')')
	$(eval PREVIOUS_VERSION_HASH := $(shell echo $(PREVIOUS_VERSION) | jq -rc '.ETag'))

	$(eval TMP_FILE_STATE := $(shell mktemp))
	aws s3api get-object --bucket $(BUCKET) --key $(REMOTE_STATE_PREFIX) --version-id $(PREVIOUS_VERSION_ID) $(TMP_FILE_STATE)

	aws s3api put-object --bucket $(BUCKET) --key $(REMOTE_STATE_PREFIX) --body $(TMP_FILE_STATE)

	$(eval TMP_FILE_LOCK := $(shell mktemp))
	echo '{"LockID":{"S":"'$(BUCKET)/$(REMOTE_STATE_PREFIX)'-md5"},"Digest":{"S":"'$(PREVIOUS_VERSION_HASH)'"}}' > $(TMP_FILE_LOCK)

	aws dynamodb put-item --table=$(TABLE) --item=file://$(TMP_FILE_LOCK)

fmt:
	terragrunt hclfmt
	terragrunt fmt -recursive .

.SILENT:
