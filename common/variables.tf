variable "project" {
  description = "Project name"
}

variable "application" {
  description = "Application name"
  type        = string
}

variable "region" {
  description = "the AWS region in which resources are created, you must set the availability_zones variable as well if you define this value to something other than the default"
}

variable "domain" {
  description = "Domain for using in project"
}

variable "runner_token" {
  default = "Gitlab CI Runner token"
}

variable "users" {
  description = "IAM users"
  type = map(
    object({
      name = string,
      role = string,
      key  = bool,
    })
  )
}

variable "roles" {
  description = "IAM roles"
  type = map(
    list(
      string
    )
  )
}

variable "containers" {
  description = "Containers"
  type        = map(object({ name = string }))
}
