variable "users" {
  description = "IAM users"
  type = map(
    object({
      name = string,
      role = string,
      key  = bool,
    })
  )
}

variable "roles" {
  description = "IAM roles"
  type = map(
    list(
      string
    )
  )
}
